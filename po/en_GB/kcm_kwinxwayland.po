# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-09 00:39+0000\n"
"PO-Revision-Date: 2022-12-31 17:43+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ui/main.qml:32
#, fuzzy, kde-format
#| msgid ""
#| "Legacy X11 apps require the ability to read keystrokes typed in other "
#| "apps for features that are activated using global keyboard shortcuts. "
#| "This is disabled by default for security reasons. If you need to use such "
#| "apps, you can choose your preferred balance of security and functionality "
#| "here."
msgid ""
"Some legacy X11 apps require the ability to read keystrokes typed in other "
"apps for certain features, such as handling global keyboard shortcuts. This "
"is allowed by default. However other features may require the ability to "
"read all keys, and this is disabled by default for security reasons. If you "
"need to use such apps, you can choose your preferred balance of security and "
"functionality here."
msgstr ""
"Legacy X11 apps require the ability to read keystrokes typed in other apps "
"for features that are activated using global keyboard shortcuts. This is "
"disabled by default for security reasons. If you need to use such apps, you "
"can choose your preferred balance of security and functionality here."

#: ui/main.qml:48
#, kde-format
msgid "Allow legacy X11 apps to read keystrokes typed in all apps:"
msgstr "Allow legacy X11 apps to read keystrokes typed in all apps:"

#: ui/main.qml:49
#, kde-format
msgid "Never"
msgstr "Never"

#: ui/main.qml:55
#, fuzzy, kde-format
#| msgid "Only Meta, Control, Alt, and Shift keys"
msgid "Only Meta, Control, Alt and Shift keys"
msgstr "Only Meta, Control, Alt, and Shift keys"

#: ui/main.qml:61
#, fuzzy, kde-format
#| msgid "All keys, but only while Meta, Ctrl, Alt, or Shift keys are pressed"
msgid ""
"As above, plus any key typed while the Control, Alt, or Meta keys are pressed"
msgstr "All keys, but only while Meta, Ctrl, Alt, or Shift keys are pressed"

#: ui/main.qml:68
#, kde-format
msgid "Always"
msgstr "Always"

#: ui/main.qml:78
#, kde-format
msgid "Additionally include mouse buttons"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."
msgstr ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."

#~ msgid ""
#~ "This module lets configure which keyboard events are forwarded to X11 "
#~ "apps regardless of their focus."
#~ msgstr ""
#~ "This module lets configure which keyboard events are forwarded to X11 "
#~ "apps regardless of their focus."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "steve.allewell@gmail.com"

#~ msgid "Legacy X11 App Support"
#~ msgstr "Legacy X11 App Support"

#~ msgid "Allow legacy X11 apps to read keystrokes typed in other apps"
#~ msgstr "Allow legacy X11 apps to read keystrokes typed in other apps"
